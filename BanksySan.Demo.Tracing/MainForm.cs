﻿namespace BanksySan.Demo.Tracing
{
    using System;
    using System.Diagnostics;
    using System.Windows.Forms;

    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            lblInformation.Text += $"{Environment.NewLine}Current PID = {Process.GetCurrentProcess().Id}";
        }

        private void btnTraceWriteLine_Click(object sender, EventArgs e)
        {
            Trace.WriteLine(txtInput.Text);
        }

        private void btnTraceError_Click(object sender, EventArgs e)
        {
            Trace.TraceError(txtInput.Text);
        }

        private void btnTraceInformation_Click(object sender, EventArgs e)
        {
            Trace.TraceInformation(txtInput.Text);
        }

        private void btnTraceWarning_Click(object sender, EventArgs e)
        {
            Trace.TraceWarning(txtInput.Text);
        }
    }
}