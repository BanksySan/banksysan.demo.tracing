﻿namespace BanksySan.Demo.Tracing
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnTraceWriteLine = new System.Windows.Forms.Button();
            this.grpTrace = new System.Windows.Forms.GroupBox();
            this.btnTraceError = new System.Windows.Forms.Button();
            this.btnTraceInformation = new System.Windows.Forms.Button();
            this.btnTraceWarning = new System.Windows.Forms.Button();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.lblMessage = new System.Windows.Forms.Label();
            this.lblInformation = new System.Windows.Forms.Label();
            this.grpTrace.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnTraceWriteLine
            // 
            this.btnTraceWriteLine.Location = new System.Drawing.Point(6, 19);
            this.btnTraceWriteLine.Name = "btnTraceWriteLine";
            this.btnTraceWriteLine.Size = new System.Drawing.Size(75, 23);
            this.btnTraceWriteLine.TabIndex = 0;
            this.btnTraceWriteLine.Text = "WriteLine()";
            this.btnTraceWriteLine.UseVisualStyleBackColor = true;
            this.btnTraceWriteLine.Click += new System.EventHandler(this.btnTraceWriteLine_Click);
            // 
            // grpTrace
            // 
            this.grpTrace.Controls.Add(this.btnTraceWarning);
            this.grpTrace.Controls.Add(this.btnTraceInformation);
            this.grpTrace.Controls.Add(this.btnTraceError);
            this.grpTrace.Controls.Add(this.btnTraceWriteLine);
            this.grpTrace.Location = new System.Drawing.Point(9, 182);
            this.grpTrace.Name = "grpTrace";
            this.grpTrace.Size = new System.Drawing.Size(350, 59);
            this.grpTrace.TabIndex = 1;
            this.grpTrace.TabStop = false;
            this.grpTrace.Text = "System.Diagnostocs.Trace";
            // 
            // btnTraceError
            // 
            this.btnTraceError.Location = new System.Drawing.Point(87, 19);
            this.btnTraceError.Name = "btnTraceError";
            this.btnTraceError.Size = new System.Drawing.Size(75, 23);
            this.btnTraceError.TabIndex = 0;
            this.btnTraceError.Text = "TraceError()";
            this.btnTraceError.UseVisualStyleBackColor = true;
            this.btnTraceError.Click += new System.EventHandler(this.btnTraceError_Click);
            // 
            // btnTraceInformation
            // 
            this.btnTraceInformation.Location = new System.Drawing.Point(168, 19);
            this.btnTraceInformation.Name = "btnTraceInformation";
            this.btnTraceInformation.Size = new System.Drawing.Size(75, 23);
            this.btnTraceInformation.TabIndex = 0;
            this.btnTraceInformation.Text = "TraceInformation()";
            this.btnTraceInformation.UseVisualStyleBackColor = true;
            this.btnTraceInformation.Click += new System.EventHandler(this.btnTraceInformation_Click);
            // 
            // btnTraceWarning
            // 
            this.btnTraceWarning.Location = new System.Drawing.Point(249, 19);
            this.btnTraceWarning.Name = "btnTraceWarning";
            this.btnTraceWarning.Size = new System.Drawing.Size(93, 23);
            this.btnTraceWarning.TabIndex = 0;
            this.btnTraceWarning.Text = "TraceWarning()";
            this.btnTraceWarning.UseVisualStyleBackColor = true;
            this.btnTraceWarning.Click += new System.EventHandler(this.btnTraceWarning_Click);
            // 
            // txtInput
            // 
            this.txtInput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInput.Location = new System.Drawing.Point(71, 143);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(523, 20);
            this.txtInput.TabIndex = 2;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(12, 146);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(53, 13);
            this.lblMessage.TabIndex = 3;
            this.lblMessage.Text = "Message:";
            // 
            // lblInformation
            // 
            this.lblInformation.AutoSize = true;
            this.lblInformation.Location = new System.Drawing.Point(15, 13);
            this.lblInformation.Name = "lblInformation";
            this.lblInformation.Size = new System.Drawing.Size(510, 52);
            this.lblInformation.TabIndex = 4;
            this.lblInformation.Text = resources.GetString("lblInformation.Text");
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 255);
            this.Controls.Add(this.lblInformation);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.txtInput);
            this.Controls.Add(this.grpTrace);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Tracing Demo";
            this.grpTrace.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTraceWriteLine;
        private System.Windows.Forms.GroupBox grpTrace;
        private System.Windows.Forms.Button btnTraceWarning;
        private System.Windows.Forms.Button btnTraceInformation;
        private System.Windows.Forms.Button btnTraceError;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblInformation;
    }
}