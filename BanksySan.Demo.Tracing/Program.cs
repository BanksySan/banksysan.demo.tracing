﻿namespace BanksySan.Demo.Tracing
{
    internal class Program
    {
        private static void Main()
        {
            var form = new MainForm();

            form.ShowDialog();
        }
    }
}