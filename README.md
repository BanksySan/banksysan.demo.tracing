# Demo of Tracing #

## Why? ##

Other solutions either require you to either restart an application with a changed config in order to attach a trace listener or add other functionality to your application for attaching trace listeners.

Sysinternal's DebugView doesn't require this as it consumes via the Windows API.

## How to use the demo ##

Requires Sysinternal's [DebugView](https://technet.microsoft.com/en-us/library/bb896647.aspx).

![The form](docs/MainForm.JPG)

Run DebugView as **Administrator** and make sure that the correct options are selected in DebugView.

![DebugView Options](docs/DebugViewOptions.jpg)

Enter your test message in the text box and click the buttons in the `System.Diagnostics.Trace` group box.

You should see your trace messages appearing in the DebugView window.

They will be labelled by the Process ID (`PID`) of the form application.  This is wrieen on the form if you can't be bothered to find it in the Task Manager.

> Do not run the form with the Visual Studio debugger attached (or any other debugger) as they will intercept the messages before the DebugView can see them.